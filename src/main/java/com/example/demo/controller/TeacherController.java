package com.example.demo.controller;

import com.example.demo.dto.StudentDTO;
import com.example.demo.dto.TeacherDTO;
import com.example.demo.entity.Teacher;
import com.example.demo.exceptionHandling.NoSuchTeacherException;
import com.example.demo.exceptionHandling.TeacherIncorrectData;
import com.example.demo.mapping.TeacherMappingUtilsImpl;
import com.example.demo.service.TeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController()
@RequestMapping("/teachers")
public class TeacherController {

    private TeacherService teacherService;

    @Autowired
    public TeacherController(TeacherService teacherService) {
        this.teacherService = teacherService;
    }

    @GetMapping
    public List<TeacherDTO> getAllTeachers(){
        return teacherService.getAllTeachers();
    }

    @GetMapping("/{id}")
    public TeacherDTO getTeacher(@PathVariable int id) {
        return teacherService.getTeacher(id);
    }

    @PostMapping
    public void addTeacher(@RequestBody Teacher teacher){
        teacherService.addTeacher(teacher);
    }

    @DeleteMapping("/{id}")
    public void deleteTeacher(@PathVariable int id){
        teacherService.deleteTeacher(id);
    }

    @GetMapping("/students/{id}")
    public List<StudentDTO> getStudentsByTeacherId(@PathVariable int id){
        return teacherService.getStudentsByTeacherId(id);
    }

    @Modifying
    @PostMapping("/{teacherId}/students/{studentId}")
    void addStudentToTeacher(@PathVariable int teacherId, @PathVariable int studentId){
        teacherService.addStudentToTeacher(studentId, teacherId);
    }

    @DeleteMapping("/{teacherId}/students/{studentId}")
    void removeStudentFromTeacher(@PathVariable int teacherId, @PathVariable int studentId){
        teacherService.removeStudentFromTeacher(studentId, teacherId);
    }

    @ExceptionHandler
    public ResponseEntity<TeacherIncorrectData> handleException(NoSuchTeacherException exception) {
        TeacherIncorrectData data = new TeacherIncorrectData();
        data.setInfo(exception.getMessage());
        return new ResponseEntity<TeacherIncorrectData>(data, NOT_FOUND);
    }

}
