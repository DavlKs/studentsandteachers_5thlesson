package com.example.demo.controller;

import com.example.demo.dto.StudentDTO;
import com.example.demo.dto.TeacherDTO;
import com.example.demo.entity.Student;
import com.example.demo.exceptionHandling.NoSuchStudentException;
import com.example.demo.exceptionHandling.StudentIncorrectData;
import com.example.demo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@RequestMapping("/students")
public class StudentController {

    private StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping
    public List<StudentDTO> getAllStudents(){
        return studentService.getAllStudents();
    }

    @GetMapping("/{id}")
    public StudentDTO getStudent(@PathVariable int id) {
        return studentService.getStudent(id);
    }

    @PostMapping
    public void addStudent(@RequestBody Student student) {
        studentService.addStudent(student);
    }

    @DeleteMapping ("/{id}")
    public void deleteStudent(@PathVariable int id) {
        studentService.deleteStudent(id);
    }

    @GetMapping("/teachers/{id}")
    public List<TeacherDTO> getTeachersByStudentId(@PathVariable int id) {
        return studentService.getTeachersByStudentId(id);
    }


    @ExceptionHandler
    public ResponseEntity<StudentIncorrectData> handleException(NoSuchStudentException exception) {
        StudentIncorrectData data = new StudentIncorrectData();
        data.setInfo(exception.getMessage());
        return new ResponseEntity<StudentIncorrectData>(data, NOT_FOUND);
    }
}
