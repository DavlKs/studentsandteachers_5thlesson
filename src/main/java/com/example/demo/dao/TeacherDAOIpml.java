package com.example.demo.dao;

import com.example.demo.entity.Student;
import com.example.demo.entity.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
public class TeacherDAOIpml implements TeacherDAO{

    private EntityManager entityManager;

    @Autowired
    public TeacherDAOIpml(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<Teacher> getAllTeachers() {
        Query query = entityManager.createQuery("from Teacher");
        return query.getResultList();
    }

    @Override
    public Teacher getTeacher(int id) {
        return entityManager.find(Teacher.class, id);
    }


    @Override
    public void addTeacher(Teacher teacher) {
        entityManager.merge(teacher);
    }

    @Override
    public void deleteTeacher(int id) {
        Query query = entityManager.createQuery("delete from Teacher where id = :teacherId");
        query.setParameter("teacherId", id);
        query.executeUpdate();
    }

    @Override
    public List<Student> getStudentsByTeacherId(int id) {
        Query query = entityManager.createNativeQuery("select * from students " +
                "join students_teachers st on students.student_id = st.student_id " +
                "where teacher_id = :teacherId", Student.class);
        query.setParameter("teacherId", id);
        return query.getResultList();
    }

    @Override
    public void addStudentToTeacher(int studentId, int teacherId) {
        Query query = entityManager.createNativeQuery("insert into students_teachers values " +
                "(:studentId, :teacherId)");
        query.setParameter("studentId", studentId);
        query.setParameter("teacherId", teacherId);
        query.executeUpdate();
    }

    @Override
    public void removeStudentFromTeacher(int studentId, int teacherId) {
        Query query = entityManager.createNativeQuery("delete from students_teachers " +
                "where student_id = :studentId AND teacher_id = :teacherId");
        query.setParameter("studentId", studentId);
        query.setParameter("teacherId", teacherId);
        query.executeUpdate();
    }
}
