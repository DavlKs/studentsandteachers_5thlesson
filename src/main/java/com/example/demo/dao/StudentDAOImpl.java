package com.example.demo.dao;

import com.example.demo.entity.Student;
import com.example.demo.entity.Teacher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
public class StudentDAOImpl implements StudentDAO {

    private EntityManager entityManager;

    @Autowired
    public StudentDAOImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public List<Student> getAllStudents() {
        Query query = entityManager.createQuery("from Student");
        return query.getResultList();
    }

    @Override
    public Student getStudent(int id) {
        return entityManager.find(Student.class, id);
    }

    @Override
    public void addStudent(Student student) {
        entityManager.merge(student);
    }

    @Override
    public void deleteStudent(int id) {
        Query query = entityManager.createQuery("delete from Student where id = :studentId");
        query.setParameter("studentId", id);
        query.executeUpdate();
    }

    @Override
    public List<Teacher> getTeachersByStudentId(int id) {
        Query query = entityManager.createNativeQuery("select * from teachers t " +
                "JOIN students_teachers st on t.teacher_id = st.teacher_id " +
                "where student_id = :studentId", Teacher.class);
        query.setParameter("studentId", id);
        return query.getResultList();
    }

}
