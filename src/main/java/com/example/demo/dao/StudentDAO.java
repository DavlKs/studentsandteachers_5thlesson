package com.example.demo.dao;

import com.example.demo.entity.Student;
import com.example.demo.entity.Teacher;

import java.util.List;

public interface StudentDAO {

    List<Student> getAllStudents();

    Student getStudent(int id);

    void addStudent(Student student);

    void deleteStudent(int id);

    List<Teacher> getTeachersByStudentId(int id);
}
