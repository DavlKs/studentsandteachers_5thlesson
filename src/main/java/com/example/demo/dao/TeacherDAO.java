package com.example.demo.dao;

import com.example.demo.entity.Student;
import com.example.demo.entity.Teacher;

import java.util.List;

public interface TeacherDAO {

    List<Teacher> getAllTeachers();

    Teacher getTeacher(int id);

    void addTeacher(Teacher teacher);

    void deleteTeacher(int id);

    List<Student> getStudentsByTeacherId(int id);

    void addStudentToTeacher(int studentId, int teacherId);

    void removeStudentFromTeacher(int studentId, int teacherId);
}
