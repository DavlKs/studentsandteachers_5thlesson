package com.example.demo.exceptionHandling;

public class TeacherIncorrectData {

    private String info;

    public TeacherIncorrectData() {
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
