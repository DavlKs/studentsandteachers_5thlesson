package com.example.demo.exceptionHandling;

public class NoSuchTeacherException extends RuntimeException {

    public NoSuchTeacherException(String message) {
        super(message);
    }
}
