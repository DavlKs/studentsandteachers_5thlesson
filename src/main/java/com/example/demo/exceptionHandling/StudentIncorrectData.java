package com.example.demo.exceptionHandling;

public class StudentIncorrectData {

    private String info;

    public StudentIncorrectData() {
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }
}
