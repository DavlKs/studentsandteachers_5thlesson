package com.example.demo.service;

import com.example.demo.dao.TeacherDAO;
import com.example.demo.dto.StudentDTO;
import com.example.demo.dto.TeacherDTO;
import com.example.demo.entity.Teacher;
import com.example.demo.exceptionHandling.NoSuchTeacherException;
import com.example.demo.mapping.StudentMappingUtilsImpl;
import com.example.demo.mapping.TeacherMappingUtilsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TeacherServiceImpl implements TeacherService {

    private TeacherDAO teacherDAO;

    @Autowired
    public TeacherServiceImpl(TeacherDAO teacherDAO) {
        this.teacherDAO = teacherDAO;
    }

    @Override
    public List<TeacherDTO> getAllTeachers() {
        return teacherDAO.getAllTeachers().stream()
                .map(TeacherMappingUtilsImpl::mapToTeacherDTO)
                .collect(Collectors.toList());
    }

    @Override
    public TeacherDTO getTeacher(int id) {
        Teacher teacher = teacherDAO.getTeacher(id);
        if (teacher == null) {
            throw new NoSuchTeacherException(
                    "There is no teacher with id = " + id + " in database");
        }
        return TeacherMappingUtilsImpl.mapToTeacherDTO(teacher);
    }

    @Transactional
    @Override
    public void addTeacher(Teacher teacher) {
        teacherDAO.addTeacher(teacher);
    }

    @Transactional
    @Override
    public void deleteTeacher(int id) {
        Teacher teacher = teacherDAO.getTeacher(id);
        if (teacher == null) {
            throw new NoSuchTeacherException(
                    "There is no teacher with id = " + id + " in database");
        }
        teacherDAO.deleteTeacher(id);
    }

    @Override
    public List<StudentDTO> getStudentsByTeacherId(int id) {
        Teacher teacher = teacherDAO.getTeacher(id);
        if (teacher == null) {
            throw new NoSuchTeacherException(
                    "There is no teacher with id = " + id + " in database");
        }
        return teacherDAO.getStudentsByTeacherId(id).stream()
                .map(StudentMappingUtilsImpl::mapToStudentDTO)
                .collect(Collectors.toList());
    }

    @Transactional
    @Override
    public void addStudentToTeacher(int studentId, int teacherId) {
        teacherDAO.addStudentToTeacher(studentId, teacherId);
    }

    @Transactional
    @Override
    public void removeStudentFromTeacher(int studentId, int teacherId) {
        teacherDAO.removeStudentFromTeacher(studentId, teacherId);
    }
}
