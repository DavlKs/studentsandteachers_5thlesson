package com.example.demo.service;

import com.example.demo.dto.StudentDTO;
import com.example.demo.dto.TeacherDTO;
import com.example.demo.entity.Teacher;

import java.util.List;

public interface TeacherService {

    List<TeacherDTO> getAllTeachers();

    TeacherDTO getTeacher(int id);

    void addTeacher(Teacher teacher);

    void deleteTeacher(int id);

    List<StudentDTO> getStudentsByTeacherId(int id);

    void addStudentToTeacher(int studentId, int teacherId);

    void removeStudentFromTeacher(int studentId, int teacherId);
}
