package com.example.demo.service;

import com.example.demo.dao.StudentDAO;
import com.example.demo.dto.StudentDTO;
import com.example.demo.dto.TeacherDTO;
import com.example.demo.entity.Student;
import com.example.demo.exceptionHandling.NoSuchStudentException;
import com.example.demo.mapping.StudentMappingUtilsImpl;
import com.example.demo.mapping.TeacherMappingUtilsImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class StudentServiceImpl  implements StudentService{

    private StudentDAO studentDAO;

    @Autowired
    public StudentServiceImpl(StudentDAO studentDAO) {
        this.studentDAO = studentDAO;
    }

    @Override
    public List<StudentDTO> getAllStudents() {
        return studentDAO.getAllStudents().stream()
                .map(StudentMappingUtilsImpl::mapToStudentDTO)
                .collect(Collectors.toList());
    }

    @Override
    public StudentDTO getStudent(int id) {
        Student student = studentDAO.getStudent(id);
        if (student == null) {
            throw new NoSuchStudentException(
                    "There is no student with id = " + id + " in database");
        }
        return StudentMappingUtilsImpl.mapToStudentDTO(student);
    }

    @Transactional
    @Override
    public void addStudent(Student student) {
        studentDAO.addStudent(student);
    }

    @Transactional
    @Override
    public void deleteStudent(int id) {
        Student student = studentDAO.getStudent(id);
        if (student == null) {
            throw new NoSuchStudentException(
                    "There is no student with id = " + id + " in database");
        }
        studentDAO.deleteStudent(id);
    }

    @Override
    public List<TeacherDTO> getTeachersByStudentId(int id) {
        Student student = studentDAO.getStudent(id);
        if (student == null) {
            throw new NoSuchStudentException(
                    "There is no student with id = " + id + " in database");
        }
        return studentDAO.getTeachersByStudentId(id).stream()
                .map(TeacherMappingUtilsImpl::mapToTeacherDTO)
                .collect(Collectors.toList());
    }
}
