package com.example.demo.service;

import com.example.demo.dto.StudentDTO;
import com.example.demo.dto.TeacherDTO;
import com.example.demo.entity.Student;


import java.util.List;

public interface StudentService {

    List<StudentDTO> getAllStudents();

    StudentDTO getStudent(int id);

    void addStudent(Student student);

    void deleteStudent(int id);

    List<TeacherDTO> getTeachersByStudentId(int id);

}
