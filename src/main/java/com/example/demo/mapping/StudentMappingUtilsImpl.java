package com.example.demo.mapping;

import com.example.demo.dto.StudentDTO;
import com.example.demo.entity.Student;

public class StudentMappingUtilsImpl {

    public static StudentDTO mapToStudentDTO(Student entity)  {
        StudentDTO dto = new StudentDTO();
        dto.setId(entity.getId());
        dto.setFirstName(entity.getFirstName());
        dto.setLastName(entity.getLastName());
        dto.setFaculty(entity.getFaculty());
        dto.setYear(entity.getYear());
        return dto;
    }

    public static  Student mapToStudentEntity(StudentDTO dto) {
        Student entity = new Student();
        entity.setId(dto.getId());
        entity.setFirstName(dto.getFirstName());
        entity.setLastName(dto.getLastName());
        entity.setFaculty(dto.getFaculty());
        entity.setYear(dto.getYear());
        return entity;
    }
}

