package com.example.demo.mapping;

import com.example.demo.dto.TeacherDTO;
import com.example.demo.entity.Teacher;

public class TeacherMappingUtilsImpl {

    public static TeacherDTO mapToTeacherDTO(Teacher entity) {
        TeacherDTO dto = new TeacherDTO();
        dto.setId(entity.getId());
        dto.setFirstName(entity.getFirstName());
        dto.setLastName(entity.getLastName());
        dto.setPatronymic(entity.getPatronymic());
        dto.setSubject(entity.getSubject());
        return dto;
    }

    public static Teacher mapToTeacherEntity(TeacherDTO dto) {
        Teacher entity = new Teacher();
        entity.setId(dto.getId());
        entity.setFirstName(dto.getFirstName());
        entity.setLastName(dto.getLastName());
        entity.setPatronymic(dto.getPatronymic());
        entity.setSubject(dto.getSubject());
        return entity;
    }
}
