CREATE TABLE students (
                          student_id SERIAL PRIMARY KEY NOT NULL,
                          first_name VARCHAR(30),
                          last_name VARCHAR(30) NOT NULL,
                          faculty VARCHAR(50) NOT NULL,
                          year INTEGER NOT NULL check (year BETWEEN 1 AND 4)
);

CREATE TABLE teachers (
                          teacher_id SERIAL PRIMARY KEY NOT NULL,
                          first_name VARCHAR(30),
                          last_name VARCHAR(30) NOT NULL,
                          patronymic VARCHAR(30),
                          subject VARCHAR NOT NULL
);

CREATE TABLE students_subjects (
                                   student_id INTEGER references students(student_id),
                                   subject_name VARCHAR(50) references teachers(subject)
);
